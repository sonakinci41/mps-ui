#!/usr/bin/python3
# -*- coding: utf-8 -*-
import gi, os
from gi.repository import Gtk
from mps_sample import mps_paketler, mps_islem, mps_terminal
gi.require_version('Gtk', '3.0')

class MpsUi(Gtk.Window):
	def __init__(self):
		Gtk.Window.__init__(self)
		#Paketler için listeler
		self.paketler = {}
		self.islem = "Paket Kur"#Paket Sil#Güncelle
		self.islem_yapilacak_paketler = []

		"""HEADER BAR"""
		#Bize bir üst bar oluşturalım
		self.ust_bar = Gtk.HeaderBar()
		#Üst barı ekleyelim
		self.set_titlebar(self.ust_bar)
		#Kapat vs butonları ekleyelim
		self.ust_bar.set_show_close_button(True)

		#Paket kur düğmesine basılınca paket kur bölümü açılacak
		self.paket_kur_dugme = Gtk.ToggleButton()
		self.paket_kur_dugme.set_active(True)
		self.paket_kur_dugme.connect("clicked",self.paket_toogle_degisti)
		self.paket_kur_dugme.set_label("Paket Kur")
		self.ust_bar.pack_start(self.paket_kur_dugme)

		#Paket sil düğmesine basınca paket sil bölümü açılacak
		self.paket_sil_dugme = Gtk.ToggleButton()
		self.paket_sil_dugme.connect("clicked",self.paket_toogle_degisti)
		self.paket_sil_dugme.set_label("Paket Sil")
		self.ust_bar.pack_start(self.paket_sil_dugme)

		#Güncelle düğmesine basınca güncelle bölümü açılacak
		self.paket_guncelle_dugme = Gtk.ToggleButton()
		self.paket_guncelle_dugme.connect("clicked",self.paket_toogle_degisti)
		self.paket_guncelle_dugme.set_label("Güncelle")
		self.ust_bar.pack_start(self.paket_guncelle_dugme)

		#Uygula düğmesine basınca uygulayacak
		self.uygula_dugme = Gtk.Button()
		self.uygula_dugme.set_sensitive(False)
		self.uygula_dugme.connect("clicked",self.uygula_dugme_basildi)
		self.uygula_dugme.set_label("Seçili Paketleri Kur")
		self.ust_bar.pack_end(self.uygula_dugme)


		"""PENCERE TEMEL"""
		#Varsayılan pencere boyutu
		self.set_default_size(640,480)

		#Pencereyi ikiye bölüyoruz alt kısma terminali koyacağız
		bolucu = Gtk.VPaned()
		bolucu.set_position(300)
		self.add(bolucu)

		#Üst ve alt kısım için birer taşıyıcı oluşturuyoruz
		ust_tasiyici = Gtk.VBox()
		alt_tasiyici = Gtk.VBox()

		#Taşıyıcıları bolduğumuz kısımlara ekleyelim
		bolucu.pack1(ust_tasiyici)
		bolucu.pack2(alt_tasiyici)

		"""UST KISIM"""
		#4 çeşit bölüm olacak giriş paketler bilgi güncelleme
		self.stack_liste = [mps_islem.IslemStack(), mps_paketler.Paketler(self)]
		#Bunun için stacka ihtiyacımız var
		self.stack = Gtk.Stack()
		#Stackın değişme tiği
		self.stack.set_transition_type(Gtk.StackTransitionType.SLIDE_LEFT_RIGHT)
		#Stackın değişme süresi 1000 iyidir
		self.stack.set_transition_duration(1000)
		#Stackları ekliyoruz
		for stackk in self.stack_liste:
			self.stack.add_titled(stackk,stackk.ad,stackk.baslik)
		#stackı pencereye ekleyelim
		ust_tasiyici.pack_start(self.stack, expand = True, fill = True, padding = 5)

		"""ALT KISIM"""
		#Terminalimizi ekleyelim
		self.term = mps_terminal.Terminal(self)
		alt_tasiyici.pack_start(self.term, expand = True, fill = True, padding = 5)


		#Açılışta yapılacak güncellemeleri bu fonskiyonda toplayacağız
		self.acilis_guncellemesi()

	def paket_toogle_degisti(self,widget):
		"""Ana aşamalar değiştiğinde bu fonisyon çalışacak"""
		basilan = widget.get_label()
		#Widget basılı mı değil mi basılıysa devam
		if widget.get_active():
			#Kategöri değiştirince seçili paket varmı bakalım adam seçtiyse boşa gitmesin
			if len(self.islem_yapilacak_paketler) != 0:
				#Eğer adamın seçtiği şeyler varsa soralım ne oluyor kaybetmek istiyormusun bunları
				soru = Gtk.MessageDialog(self,0,Gtk.MessageType.QUESTION, Gtk.ButtonsType.OK_CANCEL,"Seçili Paketler Var")
				soru.format_secondary_text("Bazı paketler işlem yapmak için seçildi devam etmeniz halinde seçili\npaketler sıfırlanacak.Devam etmek istiyor musunuz?")
				cevap = soru.run()
				#Tamam sil derse sıkıntı yok silip yola devam
				if cevap == Gtk.ResponseType.OK:
					self.islem_yapilacak_paketler = []
					self.uygula_dugme.set_sensitive(False)
					soru.destroy()
				#Yok eğer silme diyorsa devam etmemesi lazım
				else:
					soru.destroy()
					widget.set_active(False)
					return False
			#Aktif olan widgeti bulalım
			if basilan == "Paket Kur":
				self.uygula_dugme.set_label("Seçili Paketleri Kur")
				self.stack_liste[1].sutun_tur.set_title("Kurmak istediğiniz paketleri işaretleyiniz.")
				self.paket_sil_dugme.set_active(False)
				self.paket_guncelle_dugme.set_active(False)	
			elif basilan == "Paket Sil":
				self.uygula_dugme.set_label("Seçili Paketleri Sil")
				self.stack_liste[1].sutun_tur.set_title("Silmek istediğiniz paketleri işaretleyiniz.")
				self.paket_kur_dugme.set_active(False)
				self.paket_guncelle_dugme.set_active(False)
			elif basilan == "Güncelle":
				self.paket_kur_dugme.set_active(False)
				self.paket_sil_dugme.set_active(False)
			#islem değişmişse o zaman işlemi değiştirelim
			self.islem = basilan
			self.stack_liste[1].paketler_liste_doldur()


	def uygula_dugme_basildi(self, widget):
		"""Uygula düğmesine basılırsa bu çalışacak"""
		#Paket sayısı çıktı için gerekli
		paket_sayisi = len(self.islem_yapilacak_paketler)
		#Paketleri düzgün dizelimde koda ekleyelim
		paketler = " ".join(self.islem_yapilacak_paketler)
		#Paket kurulacak sa ona göre düzenliyoruz
		if self.islem == "Paket Kur":
			komut = "mps kur --ona {}".format(paketler)
			baslik = "Seçili Paketler Kurulacak"
			stack_yazi = "Seçili Paketler Kuruluyor"
			yazi = "Seçmiş olduğunuz {} paket kurulacak. Devam etmek istiyor musunuz?".format(paket_sayisi)
		#Paket silinecekse ona göre düzenliyoruz
		elif self.islem == "Paket Sil":
			komut = "mps sil --ona {}".format(paketler)
			baslik = "Seçili Paketler Silinecek"
			stack_yazi = "Seçili Paketler Siliniyor"
			yazi = "Seçmiş olduğunuz {} paket silinecek. Devam etmek istiyor musunuz?".format(paket_sayisi)
		#Son kez kullancıya soralım paketlerle ilgili işlem yapacak mı
		soru = Gtk.MessageDialog(self,0,Gtk.MessageType.QUESTION, Gtk.ButtonsType.OK_CANCEL,baslik)
		soru.format_secondary_text(yazi)
		cevap = soru.run()
		#Yapalım derse
		if cevap == Gtk.ResponseType.OK:
			soru.destroy()
			#İslem stackına gidelim
			self.stack_degistir(0)
			#Yazı atayalım stacka
			self.stack_liste[0].yazi_ata(stack_yazi)
			#İslem yapılacaklar listemizi sıfırlayalım
			self.islem_yapilacak_paketler = []
			#Düğmemizi kapatalım
			self.uygula_dugme.set_sensitive(False)
			#Komutları ekleyelim
			self.term.komutlar.append(komut)
			#Bunları milisten kurulu ve tüm paketleri tekrar almak böylece
			#Hata oranını azaltmak için yapıyorum
			self.term.komutlar.append("mps sor --dpl")
			self.term.komutlar.append("mps sor --kpl")
			#Komutu çalıştıralım
			self.term.komut_calistir()
		else:
			soru.destroy()

	def acilis_guncellemesi(self):
		"""Başlangıç güncellemelerini yapacağız"""
		self.term.komutlar.append("mps gun -H")
		self.term.komutlar.append("mps sor --dpl")
		self.term.komutlar.append("mps sor --kpl")
		self.term.komut_calistir()
		self.stack_liste[0].yazi_ata("Mps Ve Paketler Güncelleniyor...")
		#self.term.komut_calistir("mps bil inkscape")
		#self.term.komut_calistir("mps bil python3")

	def stack_degistir(self,id):
		"""Stack değiştirmek istediğimizde int göndereceğiz"""
		stack = self.stack_liste[id]
		#Başlığı değiştiriyoruz
		#self.set_title(stack.baslik)
		#Stackı değiştirelim
		self.stack.set_visible_child_name(stack.ad)

if __name__ == "__main__":
	#Program Root Olarak Başladı Mı Kontrol Ediyoruz
	if os.getuid() != 0:
		#Başlamadıysa kullanıcıya bilgi verip kapatıyoruz
		dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK,
				"Hata")
		dialog.format_secondary_text("MpsUi Çalışmak için root yetkilerine ihtiyaç duyar.")
		dialog.run()
	else:
		#Root olarak başladıysa programı çalıştırıyoruz
		pen = MpsUi()
		pen.connect("destroy", Gtk.main_quit)
		pen.show_all()
		Gtk.main()
