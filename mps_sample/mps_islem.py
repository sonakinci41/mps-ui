import gi
from gi.repository import Gtk

class IslemStack(Gtk.VBox):
	def __init__(self):
		Gtk.VBox.__init__(self)
		#Stack için bir ada ve bir başlığa ihtiyacımız var
		self.ad = "islem"
		self.baslik = "Mps-UI'a hoş geldiniz"


		spinner = Gtk.Spinner()
		self.pack_start(spinner,  expand = True, fill = True, padding = 5)
		spinner.start()

		self.yazi = Gtk.Label()
		self.pack_start(self.yazi, expand = False, fill = True, padding = 5)


	def yazi_ata(self,yazi):
		self.yazi.set_text(yazi)