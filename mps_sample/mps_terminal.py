import gi, os
from gi.repository import Vte, GLib, Gdk, Gio

class Terminal(Vte.Terminal):
	def __init__(self,ebeveyn):
		Vte.Terminal.__init__(self)
		#Ebeveyndeki değişkenlere ulaşmak için
		self.ebeveyn = ebeveyn
		#Komutları ebeveyn buraya dolduracak bitene kadar dönecek
		self.komutlar = []
		#Spawn sync ile terminali oluşturuyoruz
		self.spawn_sync(Vte.PtyFlags.DEFAULT,
						os.path.expanduser('~'),
						["/bin/bash"],
						[],
						GLib.SpawnFlags.DO_NOT_REAP_CHILD,
						None,
						None)
		#Renkler için bir paket oluşturacağız
		palet = [
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			]
		#Renklerimizi bir listeye veriyoruz
		renkler = [
			"#f8f8f2",
			"#272822",
			"#000000",
			"#f92672",
			"#a6e22e",
			"#e6db74",
			"#66d9ef",
			"#ae81ff",
			"#a1efe4",
			"#f8f8f2",
			"#000000",
			"#c61e5b",
			"#80af24",
			"#b3aa5a",
			"#50abbc",
			"#8b67cc",
			"#7fbcb3",
			"#cbcdc8",
			]
		#Paletlere renkleri ekleyelim
		for sayi in range(0,16):
			oldu_mu = palet[sayi].parse(renkler[sayi])
			if not oldu_mu:
				print("Renk okunamadı :: {}".format(renkler[sayi]))

		#Renkleri terminale ekleyelim
		self.set_colors(palet[0],palet[1],palet[2:])
		#Scroll hareket ettiğinde sinyal yayılacak bizde bittimi anlayacağız
		self.connect("text-inserted", self.komut_bitti)

	def komut_bitti(self,term):
		"""Komutun bitip bitmediğini anlamaya çalışacağız"""
		#Scroll hareket edince ekrandaki texti almalıyız
		text = self.get_text_include_trailing_spaces(None,None)[0]
		#Satırlara bölelim
		bol = text.split("\n")
		#Son satırı almaya çalışacağız
		bol = bol[-2]
		#Bu son satır root[ arada birşeyler ]# ise o zaman tamam bitmiş
		if bol[:6] == "root [" and bol[-3:] == "]# " and len(self.komutlar) != 0:
			#Bittiyse dosya yazımı tamamlanmıştı dosyayı okuyalım
			okunan = self.takip_oku()
			#Çalışan komut komutların 0. arkadaşı
			calisan_komut = self.komutlar[0]
			#Her komut çalıştığında farklı sonuçlar olacak bu yüzden
			if calisan_komut == "mps sor --dpl":
				self.komut_parse_dpl(okunan)
			elif calisan_komut == "mps sor --kpl":
				self.komut_parse_kpl(okunan)

			#Son çalışan komutları sileceğiz
			self.komutlar.remove(calisan_komut)
			#Çalıştıracak komut bitmediyse tekrardan komutu siliyoruz
			if len(self.komutlar) != 0:
				self.komut_calistir()
			else:
				#Komut bitince paket ekranına atıyoruz
				self.ebeveyn.stack_degistir(1)
				print("Komutlar Bitti")

	def komut_parse_dpl(self,okunan):
		"""mps sur --dpl komutuyla var olan paketleri alıp listemize
		yazıyoruz tabi önce listeyi sıfırladıktan sonra"""
		self.ebeveyn.paketler = {}
		#Geleni satirlara ayiralim
		bol = okunan.split("\n")
		for satir in bol:
			#paket_adi sürüm devir geliyor
			asama = satir.split()
			if len(asama) == 3:
				self.ebeveyn.paketler[asama[0]] = {"sürüm":asama[1],
												"devir":asama[2],
												"durum":"Kurulu Değil"}

	def komut_parse_kpl(self,okunan):
		"""mps sor --kpl komutu çalıştıktan sonra paket kurulu
		olarak işaretleyeceğiz"""
		#Geleni satirlara ayiralim
		bol = okunan.split("\n")
		for satir in bol:
			#paket_adi sürüm devir geliyor
			asama = satir.split()
			if len(asama):
				self.ebeveyn.paketler[asama[0]]["durum"] = "Kurulu"
		#PAKETLER HAZIR DEMEK PAKET PENCEREYE GEÇİŞ YAPALIM
		self.ebeveyn.stack_liste[1].paketler_liste_doldur()


	def takip_oku(self):
		"""Takip ettiğimiz dosyayı okumaya çalışacağız"""
		try:
			f = open("/tmp/t_takip.txt")
			okunan = f.read()
			f.close()
		except:
			print("/tmp/t_takip.txt OKUNAMADI")
			okunan = ""
		return okunan


	def komut_calistir(self):
		"""Komut çalıştırılmak için bu fonksiyonu yazacağız"""
		#Terminali tekrardan güncelelyelim birşeyler yazan olur falan
		a = self.spawn_sync(Vte.PtyFlags.DEFAULT,
						"/",
						["/bin/bash"],
						[],
						GLib.SpawnFlags.DO_NOT_REAP_CHILD,
						None,
						None)
		#Yürütülen komutu print edelim
		print("Komut yürütülüyor :: {} - id :: {}".format(self.komutlar[0],a))
		#Komutun çıktısını alabilmek için çıktıyı tmp altında t_takip.txt ye
		#Yazdıracağız o yüzden gelen komutu güncelliyoruz
		komut = "{} 2>&1 | tee /tmp/t_takip.txt\n".format(self.komutlar[0])
		# Feed Child versiyonu sıkıntı bu yüzden 2 farklı yöntem deniyoruz
		try:
			a = self.feed_child_binary(bytes(komut,"utf-8"))
		except:
			a = self.feed_child(komut,len(komut)+2)
