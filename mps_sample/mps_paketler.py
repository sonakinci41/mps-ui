import gi, os
from gi.repository import Gtk, GdkPixbuf

class Paketler(Gtk.VBox):
	def __init__(self,ebeveyn):
		Gtk.VBox.__init__(self)
		#Paket için bir ada ve bir başlığa ihtiyacımız var
		self.ad = "paketler"
		self.baslik = "Paket işlemleri yapabilirsiniz"
		self.ebeveyn = ebeveyn

		#Paket araması için bir entry oluşturuyoruz
		self.arama_kismi = Gtk.SearchEntry()
		self.arama_kismi.connect("search-changed",self.arama_degisti)
		self.pack_start(self.arama_kismi, expand = False, fill = True, padding = 5)
		#List store ve sinyallerini oluşturalım
		self.paketler_store = Gtk.ListStore(GdkPixbuf.Pixbuf(),str,bool)
		self.paketler_liste = Gtk.TreeView(model = self.paketler_store)
		self.paketler_liste.connect("row-activated",self.paketler_liste_tiklandi)
		#Sutunları oluşturup view a ekleyleim
		sutun_icon = Gtk.CellRendererPixbuf()
		sutun_icon.set_fixed_size(32,32)
		sutun = Gtk.TreeViewColumn("Icon",sutun_icon, gicon = 0)
		self.paketler_liste.append_column(sutun)
		sutun_text = Gtk.CellRendererText()
		sutun = Gtk.TreeViewColumn("Paket Adı",sutun_text, text = 1)
		self.paketler_liste.append_column(sutun)
		sutun_toogle = Gtk.CellRendererToggle()
		#sutun_toogle.set_radio(True)
		sutun_toogle.connect("toggled",self.kur_sil_tiklandi)
		self.sutun_tur = Gtk.TreeViewColumn("Kurmak istediğiniz paketleri işaretleyiniz.",sutun_toogle, active = 2)
		self.paketler_liste.append_column(self.sutun_tur)

		#Scrool olması lazım scrolu oluşturup view'ı verelim
		scroll = Gtk.ScrolledWindow()
		scroll.set_policy(Gtk.PolicyType.AUTOMATIC,Gtk.PolicyType.AUTOMATIC)
		scroll.add(self.paketler_liste)
		#Scroola ekleyelim
		self.pack_start(scroll, True, True, 5)

	def kur_sil_tiklandi(self, widget, path):
		satir = self.paketler_store[path]
		if satir[2]:
			#Kurulacak yada silinecek paketler arasından paketi silinecek	
			satir[2] = False
			self.ebeveyn.islem_yapilacak_paketler.remove(satir[1])
		else:
			#Kurulacak yada silinecek paketlere ekle
			satir[2] = True
			self.ebeveyn.islem_yapilacak_paketler.append(satir[1])
		if len(self.ebeveyn.islem_yapilacak_paketler) == 0:
			self.ebeveyn.uygula_dugme.set_sensitive(False)
		else:
			self.ebeveyn.uygula_dugme.set_sensitive(True)
		print(self.ebeveyn.islem_yapilacak_paketler)

	def arama_degisti(self,aranan):
		"""Aranan Değiştiğinde bu fonksiyon çalışacak"""
		#Widgettten yazıyı alalım
		aranan = aranan.get_text()
		#Bulunan paketleri buraya dolduracağız
		paket_liste = []
		#Tüm paketleri alıp döngüye verelim
		paketler = self.ebeveyn.paketler.keys()
		for paket in paketler:
			#Aranan kelime bizim paket adının içine varsa
			if aranan in paket:
				#Paketleri ekleyelim
				paket_liste.append(paket)
		#Doldursun diye fonksiyona verelim
		self.paketler_liste_doldur(paket_liste)



	def paketler_liste_doldur(self, paketler=None):
		"""Paketleri listeye ekleyeceğimiz fonksiyon bu"""
		#Paketler None ise paketler sözlüğünden keysleri alıyoruz
		if paketler == None:
			paketler = self.ebeveyn.paketler.keys()
		#Storu temizleyelim
		self.paketler_store.clear()
		#Varsayılan simge temasını alalım
		simge_tema = Gtk.IconTheme.get_default()
		#Paketleri döngüye verelim
		for paket in paketler:
			#Eğer Paket Kur işlemi yapılacaksa Kurulu paketleri görmemeiz gerek yok
			if self.ebeveyn.islem == "Paket Kur":
				if self.ebeveyn.paketler[paket]["durum"] == "Kurulu":
					continue
			#Eğer paket Silinecekse Kurulu olmayan paketleri görmeye gerek yok
			if self.ebeveyn.islem == "Paket Sil":
				if self.ebeveyn.paketler[paket]["durum"] == "Kurulu Değil":
					continue
			#Şartlarda sıkıntı yoksa paketi ekleyeceğiz
			try:
				#Temada icon varmı almayı deniyoruz
				icon = simge_tema.load_icon(paket,32,Gtk.IconLookupFlags.FORCE_SIZE)
			except:
				#Yoksa varolan simgemizi ekliyoruz
				if os.path.exists("/usr/share/icons/hicolor/32x32/apps/paket.png"):
					icon = GdkPixbuf.Pixbuf.new_from_file("/usr/share/icons/hicolor/32x32/apps/paket.png")
				else:
					icon = GdkPixbuf.Pixbuf.new_from_file("./mps_icons/paket.png")
			#İşlem tama Stora ekleyelim
			if paket in self.ebeveyn.islem_yapilacak_paketler:
				self.paketler_store.append([icon,paket,True])
			else:
				self.paketler_store.append([icon,paket,False])
		#Seçili olarak 0.yı yapalım
		self.paketler_liste.set_cursor(0)

	def paketler_liste_tiklandi(self,widget,path,coloumn):
		"""Paket listeye çift tıklanınca olacak işleri yapacağız"""
		print("Tiklandi")
