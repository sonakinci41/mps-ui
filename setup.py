#!/usr/bin/python3

from setuptools import setup, find_packages

datas = [("/usr/share/applications",["mps_data/mps_ui.desktop"]),
		("/usr/share/icons/hicolor/32x32/apps", ["mps_icons/paket.png"]),
		("/usr/share/icons/hicolor/scalable/apps",["mps_icons/paket.svg"])]


setup(
	name = "mps_ui",
	scripts = ["mps_ui.py"],
	packages = find_packages(),
	version = "0.1",
	description = "SoNAkıncı",
	author = ["Fatih Kaya"],
	author_email = "sonakinci41@gmail.com",
	url = "https://gitlab.com/sonakinci41/mps-ui",
	data_files = datas
)
